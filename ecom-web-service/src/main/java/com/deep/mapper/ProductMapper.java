package com.deep.mapper;

import org.springframework.stereotype.Component;

import com.deep.constants.EcomConstants;
import com.deep.model.Product;
import com.deep.model.ProductDetails;
import com.deep.util.DateUtil;

/**
 * The Class ProductMapper.
 */
@Component
public class ProductMapper {
	
	/**
	 * Map model to db object.
	 * @param productDetails the product details
	 * @return the product
	 */
	public Product mapModelToDbObject(ProductDetails productDetails)
	{
		Product product = new Product();
		product.setName(productDetails.getName());
		product.setPrice(productDetails.getPrice());
		product.setDate(DateUtil.formatStringToTimestamp(productDetails.getDate(), EcomConstants.DATE_FORMAT_PRODUCT));
		return product;
	}

}
