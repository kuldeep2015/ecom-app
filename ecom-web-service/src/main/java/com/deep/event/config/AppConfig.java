package com.deep.event.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * The Class AppConfig.
 */
@Component
public class AppConfig {

	/** The topic product. */
	@Value("${kafka-topic-product}")
	public String topicProduct;
	
	/** The topic db retry. */
	@Value("${kafka-topic-db-retry}")
	private String topicDbRetry;
	
	/** The kafka group id. */
	@Value("${kafka-group-id}")
	private String kafkaGroupId;
	
	/** The bootstrep servers. */
	@Value("${spring.kafka.producer.bootstrap-servers}")
	private String bootstrepServers;
	
	/** The email to. */
	@Value("${email-to}")
	private String emailTo;
	
	/** The email db failure subject. */
	@Value("${email-db-failure-subject}")
	private String emailDbFailureSubject;
	
	/**
	 * Gets the topic product.
	 * @return the topic product
	 */
	public String getTopicProduct() {
		return topicProduct;
	}

	/**
	 * Sets the topic product.
	 * @param topicProduct the new topic product
	 */
	public void setTopicProduct(String topicProduct) {
		this.topicProduct = topicProduct;
	}

	/**
	 * Gets the topic db retry.
	 * @return the topic db retry
	 */
	public String getTopicDbRetry() {
		return topicDbRetry;
	}

	/**
	 * Sets the topic db retry.
	 * @param topicDbRetry the new topic db retry
	 */
	public void setTopicDbRetry(String topicDbRetry) {
		this.topicDbRetry = topicDbRetry;
	}

	/**
	 * Gets the kafka group id.
	 * @return the kafka group id
	 */
	public String getKafkaGroupId() {
		return kafkaGroupId;
	}

	/**
	 * Sets the kafka group id.
	 * @param kafkaGroupId the new kafka group id
	 */
	public void setKafkaGroupId(String kafkaGroupId) {
		this.kafkaGroupId = kafkaGroupId;
	}

	/**
	 * Gets the bootstrep servers.
	 * @return the bootstrep servers
	 */
	public String getBootstrepServers() {
		return bootstrepServers;
	}

	/**
	 * Sets the bootstrep servers.
	 * @param bootstrepServers the new bootstrep servers
	 */
	public void setBootstrepServers(String bootstrepServers) {
		this.bootstrepServers = bootstrepServers;
	}

	/**
	 * Gets the email to.
	 * @return the email to
	 */
	public String getEmailTo() {
		return emailTo;
	}

	/**
	 * Sets the email to.
	 * @param emailTo the new email to
	 */
	public void setEmailTo(String emailTo) {
		this.emailTo = emailTo;
	}

	/**
	 * Gets the email db failure subject.
	 * @return the email db failure subject
	 */
	public String getEmailDbFailureSubject() {
		return emailDbFailureSubject;
	}

	/**
	 * Sets the email db failure subject.
	 * @param emailDbFailureSubject the new email db failure subject
	 */
	public void setEmailDbFailureSubject(String emailDbFailureSubject) {
		this.emailDbFailureSubject = emailDbFailureSubject;
	}
}
