package com.deep.event.consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import com.deep.service.ProductService;

/**
 * The Class ProductEventConsumer.
 */
@Component
public class ProductEventConsumer {
	
	/** The logger. */
	private final Logger logger = LoggerFactory.getLogger(ProductEventConsumer.class);

	/** The product service. */
	@Autowired
	ProductService productService;
	
	/**
	 * Consume message.
	 * @param message the message
	 */
	@KafkaListener(topics = "${kafka-topic-product}", groupId = "${kafka-group-id}")
	public void consumeMessage(String message)
	{
		logger.info(String.format("Consumed message from Kafka , Topic Message:- %s", message));
		productService.saveProduct(message);
	}
	
	
}
