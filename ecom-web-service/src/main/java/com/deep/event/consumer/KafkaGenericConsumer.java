package com.deep.event.consumer;

import java.util.Collections;
import java.util.Properties;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.common.serialization.LongDeserializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import com.deep.event.config.AppConfig;
import com.deep.service.EventMessageService;

/**
 * The Class KafkaGenericConsumer.
 */
@Component
public class KafkaGenericConsumer {

	/** The logger. */
	Logger logger = LoggerFactory.getLogger(KafkaGenericConsumer.class);
	
	/** The app config. */
	@Autowired
	AppConfig appConfig;
	
	/** The event message service. */
	@Autowired
	EventMessageService eventMessageService;
	
	/**
	 * Creates the consumer.
	 * @return the consumer
	 */
	public   Consumer<Long, String> createConsumer() {
	      final Properties props = new Properties();
	      props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, appConfig.getBootstrepServers());
	      props.put(ConsumerConfig.GROUP_ID_CONFIG, appConfig.getKafkaGroupId());
	      props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, LongDeserializer.class.getName());
	      props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
	      // Create the consumer using props.
	      final Consumer<Long, String> consumer = new KafkaConsumer<Long, String>(props);
	      // Subscribe to the topic.
	      consumer.subscribe(Collections.singletonList(appConfig.getTopicDbRetry()));
	      return consumer;
	  }
	
	
	/**
	 * Run consumer.
	 * @throws InterruptedException the interrupted exception
	 */
	@Scheduled(fixedDelay = 50000)
	 private void runConsumer() throws InterruptedException {
		 logger.info("----------------- STARTS Scheduled Run consumer method  ----------------------");
	        final Consumer<Long, String> consumer = createConsumer();
	        final int giveUp = 5;   int noRecordsCount = 0;
	        while (true) {
	            final ConsumerRecords<Long, String> consumerRecords = consumer.poll(1000);
	            if (consumerRecords.count()==0) {
	                noRecordsCount++;
	                if (noRecordsCount > giveUp) break;
	                else continue;
	            }
	            consumerRecords.forEach(record -> {
	            	String message = record.value();
	            	logger.info(String.format("Consumer Record:(%d, %s, %d, %d)\n",
	                        record.key(), message,record.partition(), record.offset()));
	            	eventMessageService.pushMessage(message, appConfig.getTopicProduct());
	            });
	            consumer.commitAsync();
	        }
	        consumer.close();
	        logger.info("----------------- ENDS Scheduled Run consumer method  ----------------------");
	    }
}
