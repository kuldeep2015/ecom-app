package com.deep.event.producer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

/**
 * The Class EventMessageProducer.
 */
@Service
public class EventMessageProducer {

	/** The logger. */
	Logger logger = LoggerFactory.getLogger(EventMessageProducer.class);
	
	/** The kafka template. */
	@Autowired
	private KafkaTemplate<String, String> kafkaTemplate;
	
	/**
	 * Push message to topic.
	 * @param message the message
	 * @param topicName the topic name
	 */
	public void pushMessageToTopic(String message, String topicName){
		logger.info("Pushing message to Kafka topic %s " + topicName);
		kafkaTemplate.send(topicName, message);
	}
	
	
}
