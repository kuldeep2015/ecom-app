package com.deep.util;

import com.google.gson.Gson;

/**
 * The Class JsonUtil.
 */
public class JsonUtil {

	/**
	 * Convert json to object.
	 * @param <T> the generic type
	 * @param json the json
	 * @param object the object
	 * @return the t
	 */
	public static <T>  T convertJsonToObject(String json, T object)
	{
		Gson gson = new Gson();
		T jsonObject = (T)gson.fromJson(json, object.getClass());
		return jsonObject;
	}
	
	/**
	 * Convert object to json.
	 * @param <T> the generic type
	 * @param object the object
	 * @return the string
	 */
	public static <T> String convertObjectToJson(T object){
		Gson gson = new Gson();
		String json = gson.toJson(object);
		return json;
	}
}
