package com.deep.util;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.deep.constants.EcomConstants;

/**
 * The Class DateUtil.
 */
public class DateUtil {

	/** The Constant logger. */
	private final static Logger logger = LoggerFactory.getLogger(DateUtil.class);
	
	/**
	 * Format string to timestamp.
	 * @param str_date the str date
	 * @param dateFormat the date format
	 * @return the timestamp
	 */
	public static Timestamp formatStringToTimestamp(String str_date, String dateFormat)
	{
		try {
		      DateFormat formatter = new SimpleDateFormat(EcomConstants.DATE_FORMAT_PRODUCT);
		      Date date = (Date) formatter.parse(str_date);
		      Timestamp timeStampDate = new Timestamp(date.getTime());
		      
		      return timeStampDate;
		    } catch (ParseException e) {
		    	logger.error("Exception :" + e);
		      throw new RuntimeException("Invalid date format:- " + str_date);
		    }

	}
}
