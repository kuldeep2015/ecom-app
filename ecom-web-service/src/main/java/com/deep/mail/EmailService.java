package com.deep.mail;

/**
 * The Interface EmailService.
 * @author kuldeep
 */
public interface EmailService {
    
    /**
     * Send simple message.
     * @param to the to
     * @param subject the subject
     * @param text the text
     */
    void sendSimpleMessage(String to,String subject,String text);
}