package com.deep.constants;

/**
 * The Interface EcomConstants.
 */
public interface EcomConstants {

	/** The date format product. */
	String DATE_FORMAT_PRODUCT = "dd-MM-yyyy hh:mm:ss";
	
}
