package com.deep.model;

import com.deep.constants.EcomConstants;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * The Class ProductDetails.
 */
public class ProductDetails {
	
	/** The product id. */
	private Long productId;
	
	/** The name. */
	private String name;
	
	/** The price. */
	private Double price;
	
	/** The date. */
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = EcomConstants.DATE_FORMAT_PRODUCT)
	private String date;
	
	/** The type. */
	private String type;
	
	/**
	 * Gets the product id.
	 * @return the product id
	 */
	public Long getProductId() {
		return productId;
	}
	
	/**
	 * Sets the product id.
	 * @param productId the new product id
	 */
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	
	/**
	 * Gets the name.
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Sets the name.
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Gets the price.
	 * @return the price
	 */
	public Double getPrice() {
		return price;
	}
	
	/**
	 * Sets the price.
	 * @param price the new price
	 */
	public void setPrice(Double price) {
		this.price = price;
	}
	
	/**
	 * Gets the date.
	 * @return the date
	 */
	public String getDate() {
		return date;
	}
	
	/**
	 * Sets the date.
	 * @param date the new date
	 */
	public void setDate(String date) {
		this.date = date;
	}
	
	/**
	 * Gets the type.
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	
	/**
	 * Sets the type.
	 * @param type the new type
	 */
	public void setType(String type) {
		this.type = type;
	}
	
}
