package com.deep.model;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The Class Product.
 */
@Entity
@Table(name="product")
public class Product implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6026964650694814326L;
	
	/** The product id. */
	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	@Column(name ="product_id" , updatable = false, nullable = false)
	private Long productId;
	
	/** The name. */
	@Column(name ="name")
	private String name;
	
	/** The price. */
	@Column(name ="price")
	private Double price;
	
	/** The date. */
	@Column(name ="date")
	private Timestamp date;
	
	/** The type. */
	@Column(name ="type")
	private String type;
	
	
	/**
	 * Gets the product id.
	 * @return the product id
	 */
	public Long getProductId() {
		return productId;
	}
	
	/**
	 * Sets the product id.
	 * @param productId the new product id
	 */
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	
	/**
	 * Gets the name.
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Sets the name.
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Gets the price.
	 * @return the price
	 */
	public Double getPrice() {
		return price;
	}
	
	/**
	 * Sets the price.
	 * @param price the new price
	 */
	public void setPrice(Double price) {
		this.price = price;
	}
	
	/**
	 * Gets the date.
	 * @return the date
	 */
	public Timestamp getDate() {
		return date;
	}
	
	/**
	 * Sets the date.
	 * @param date the new date
	 */
	public void setDate(Timestamp date) {
		this.date = date;
	}
	
	/**
	 * Gets the type.
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	
	/**
	 * Sets the type.
	 * @param type the new type
	 */
	public void setType(String type) {
		this.type = type;
	}
	
	
	}
