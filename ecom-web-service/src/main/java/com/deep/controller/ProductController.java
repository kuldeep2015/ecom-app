package com.deep.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.deep.event.config.AppConfig;
import com.deep.model.ProductDetails;
import com.deep.service.EventMessageService;
import com.deep.service.ProductService;

/**
 * The Class ProductController.
 */
@RestController
@RequestMapping("/product")
public class ProductController {

	/** The product service. */
	@Autowired
	ProductService productService;
	
	/** The event message service. */
	@Autowired
	EventMessageService eventMessageService;
	
	/** The app config. */
	@Autowired
	AppConfig appConfig;
	
	/**
	 * Save product.
	 * @param productDetails the product details
	 */
	@PostMapping(value="save", produces=MediaType.APPLICATION_JSON_VALUE)
	public void saveProduct(@RequestBody ProductDetails productDetails){
		eventMessageService.convertAndPushMessage(productDetails, appConfig.getTopicProduct());
	}
	
	/**
	 * Save product to db topic.
	 * @param productDetails the product details
	 */
	@PostMapping(value="saveToDbTopic", produces=MediaType.APPLICATION_JSON_VALUE)
	public void saveProductToDbTopic(@RequestBody ProductDetails productDetails){
		eventMessageService.convertAndPushMessage(productDetails, appConfig.getTopicDbRetry());
	}
		
}
