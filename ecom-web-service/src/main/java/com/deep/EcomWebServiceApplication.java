package com.deep;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * The Class EcomWebServiceApplication.
 */
@SpringBootApplication
@EnableScheduling
public class EcomWebServiceApplication {

	/**
	 * The main method.
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		SpringApplication.run(EcomWebServiceApplication.class, args);
	}

}
