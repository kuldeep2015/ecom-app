package com.deep.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.deep.event.producer.EventMessageProducer;
import com.deep.util.JsonUtil;

/**
 * The Class EventMessageService.
 */
@Service
public class EventMessageService {

	/** The logger. */
	Logger logger = LoggerFactory.getLogger(EventMessageService.class);
	
	/** The event message producer. */
	@Autowired
	EventMessageProducer eventMessageProducer;
	
	/**
	 * Convert and push message.
	 * @param <T> the generic type
	 * @param object the object
	 * @param topicName the topic name
	 */
	public <T> void  convertAndPushMessage(T object, String topicName){
		String jsonMessage = JsonUtil.convertObjectToJson(object);
		logger.info("Convert and Sending message to EventMessageProducer for Kafka topic " + topicName);
		eventMessageProducer.pushMessageToTopic(jsonMessage, topicName);
	}
	
	/**
	 * Push message.
	 * @param jsonMessage the json message
	 * @param topicName the topic name
	 */
	public void  pushMessage(String jsonMessage, String topicName){
		logger.info("Sending message to EventMessageProducer for Kafka topic " + topicName);
		eventMessageProducer.pushMessageToTopic(jsonMessage, topicName);
	}
	
}
