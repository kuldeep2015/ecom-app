package com.deep.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.deep.model.Product;

/**
 * The Interface ProductRepository.
 */
@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

	/**
	 * Find by product id.
	 * @param id the id
	 * @return the product
	 */
	Product findByProductId(Long id);
}
