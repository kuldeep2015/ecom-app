package com.deep.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;

import com.deep.constants.EcomConstants;
import com.deep.event.config.AppConfig;
import com.deep.event.producer.EventMessageProducer;
import com.deep.mail.EmailService;
import com.deep.mapper.ProductMapper;
import com.deep.model.Product;
import com.deep.model.ProductDetails;
import com.deep.repository.ProductRepository;
import com.deep.util.DateUtil;
import com.deep.util.JsonUtil;
import static org.mockito.Matchers.anyString;

@RunWith(SpringRunner.class)
public class ProductServiceTest {

	@MockBean
	ProductRepository productRepository;
	@MockBean
	ProductMapper productMapper;
	@MockBean
	EventMessageProducer eventMessageProducer;
	@MockBean
	AppConfig appConfig;
	@MockBean
	EmailService emailServiceImpl;
	@Autowired
	ProductService productService;
	
	@TestConfiguration
    static class ProductServiceTestContextConfiguration {
  
        @Bean
        public ProductService productService() {
            return new ProductService();
        }
    }
	
	@Test
	public void testSaveProduct() {
		ProductDetails productDetails = new ProductDetails();
		String productJson = "{\"name\":\"Pepsi\",\"type\":\"drink\",\"price\":50,\"date\":\"18-08-2019 19:32:51\"}";
		productDetails = JsonUtil.convertJsonToObject(productJson, productDetails);
		
		Product product = new Product();
		product.setName("Pepsi");
		product.setPrice(50.0);
		product.setDate(DateUtil.formatStringToTimestamp("18-08-2019 19:32:51", EcomConstants.DATE_FORMAT_PRODUCT));
		
		Product newProduct = new Product();
		product.setName("Pepsi");
		product.setPrice(50.0);
		product.setDate(DateUtil.formatStringToTimestamp("18-08-2019 19:32:51", EcomConstants.DATE_FORMAT_PRODUCT));
		product.setProductId(1l);
		
		given(productMapper.mapModelToDbObject(productDetails)).willReturn(product);
		given(productRepository.save(product)).willReturn(newProduct);
		productService.saveProduct(productJson);
	}

	@Test
	public void testSaveProduct_when_dbError() {
		ProductDetails productDetails = new ProductDetails();
		String productJson = "{\"name\":\"Pepsi\",\"type\":\"drink\",\"price\":50,\"date\":\"18-08-2019 19:32:51\"}";
		productDetails = JsonUtil.convertJsonToObject(productJson, productDetails);
		
		Product product = new Product();
		product.setName("Pepsi");
		product.setPrice(50.0);
		product.setDate(DateUtil.formatStringToTimestamp("18-08-2019 19:32:51", EcomConstants.DATE_FORMAT_PRODUCT));
				
		given(productMapper.mapModelToDbObject(productDetails)).willReturn(product);
		given(productRepository.save(product)).willThrow(new RuntimeException());
		
		doNothing().when(emailServiceImpl).sendSimpleMessage(anyString(), anyString(), anyString());
		doNothing().when(eventMessageProducer).pushMessageToTopic(anyString(), anyString());
		
		productService.saveProduct(productJson);
	}
}
