package com.deep.mapper;

import static org.junit.Assert.*;
import org.hamcrest.MatcherAssert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import com.deep.constants.EcomConstants;
import com.deep.model.Product;
import com.deep.model.ProductDetails;
import com.deep.service.ProductService;
import com.deep.util.DateUtil;
import com.deep.util.JsonUtil;

@RunWith(SpringRunner.class)
public class ProductMapperTest {

	@Autowired
	ProductMapper productMapper;
	
	@TestConfiguration
    static class ProductMapperTestContextConfiguration {
  
        @Bean
        public ProductMapper productMapper() {
            return new ProductMapper();
        }
    }
	
	@Test
	public void testMapModelToDbObject() {
		
		
		
		ProductDetails productDetails = new ProductDetails();
		String productJson = "{\"name\":\"Pepsi\",\"type\":\"drink\",\"price\":50,\"date\":\"18-08-2019 19:32:51\"}";
		productDetails = JsonUtil.convertJsonToObject(productJson, productDetails);
		
		Product product = new Product();
		product.setName("Pepsi");
		product.setPrice(50.0);
		product.setDate(DateUtil.formatStringToTimestamp("18-08-2019 19:32:51", EcomConstants.DATE_FORMAT_PRODUCT));
		
		Product updatedProduct = productMapper.mapModelToDbObject(productDetails);
		assertEquals(product.getName(), updatedProduct.getName());
		
	}

}
