package com.deep.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.deep.event.config.AppConfig;
import com.deep.model.ProductDetails;
import com.deep.service.EventMessageService;
import com.deep.service.ProductService;
import com.google.gson.Gson;

@RunWith(SpringRunner.class)
@WebMvcTest
public class ProductControllerTest {

	@Autowired
    private MockMvc mockMvc;
	
	@MockBean
	EventMessageService eventMessageService;
	@MockBean
	AppConfig appConfig;
	@MockBean
	ProductService productService;
	
	@Test
	public void testSaveProduct() throws Exception {
		ProductDetails productDetails = new ProductDetails();
		productDetails.setDate("18-08-2019 19:32:51");
		productDetails.setName("Pepsi");
		productDetails.setPrice(50.50);
		
		given(appConfig.getTopicProduct()).willReturn("TOPIC-PRODUCT");
		doNothing().when(eventMessageService).convertAndPushMessage(productDetails, "TOPIC-PRODUCT");
		
		Gson gson = new Gson();
	    String json = gson.toJson(productDetails);
		
		this.mockMvc.perform(post("/product/save").
				contentType(MediaType.APPLICATION_JSON)
                .content(json))
        .andExpect(status().isOk()).andReturn();
	}

	@Test
	public void testSaveToDbTopic() throws Exception {
		ProductDetails productDetails = new ProductDetails();
		productDetails.setDate("18-08-2019 19:32:51");
		productDetails.setName("Pepsi");
		productDetails.setPrice(50.50);
		
		given(appConfig.getTopicProduct()).willReturn("TOPIC-DB-RETRY");
		doNothing().when(eventMessageService).convertAndPushMessage(productDetails, "TOPIC-DB-RETRY");
		
		Gson gson = new Gson();
	    String json = gson.toJson(productDetails);
		
		this.mockMvc.perform(post("/product/saveToDbTopic").
				contentType(MediaType.APPLICATION_JSON)
                .content(json))
        .andExpect(status().isOk()).andReturn();
	}
}
