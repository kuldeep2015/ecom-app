package com.deep.mail;

import static org.mockito.Mockito.doNothing;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class EmailServiceImplTest {

	@MockBean
	public JavaMailSender emailSender;
	@Autowired
	EmailService emailServiceImpl;
	
	@TestConfiguration
    static class EmailServiceImplTestContextConfiguration {
  
        @Bean
        public EmailService emailServiceImpl() {
            return new EmailServiceImpl();
        }
    }
	
	@Test
	public void testSendSimpleMessage() {
		SimpleMailMessage message = new SimpleMailMessage();
        String to = "abc@test.com";
        String subject = "test-Subject";
        String text = "Message Body";
		message.setTo(to);
        message.setSubject(subject);
        message.setText(text);
        doNothing().when(emailSender).send(message);
        emailServiceImpl.sendSimpleMessage(to, subject, text);
	}

}
