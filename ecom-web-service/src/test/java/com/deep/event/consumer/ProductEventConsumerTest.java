package com.deep.event.consumer;

import static org.mockito.Mockito.doNothing;

import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.kafka.test.rule.KafkaEmbedded;
import org.springframework.test.context.junit4.SpringRunner;

import com.deep.model.ProductDetails;
import com.deep.service.EventMessageService;
import com.deep.service.ProductService;
import com.deep.util.JsonUtil;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductEventConsumerTest {

	private static String BOOT_TOPIC = "TOPIC-PRODUCT";
	@Autowired
	ProductEventConsumer productEventConsumer;
	@Autowired
	EventMessageService eventMessageService;
	@MockBean
	ProductService productService;
	
	@ClassRule
	public static KafkaEmbedded embeddedKafka = new KafkaEmbedded(1, true, BOOT_TOPIC);
	
	@Test
	public void testConsumeMessage() {
		String productJson = "{\"name\":\"Pepsi\",\"type\":\"drink\",\"price\":50,\"date\":\"18-08-2019 19:32:51\"}";
		eventMessageService.pushMessage(productJson, BOOT_TOPIC);
		doNothing().when(productService).saveProduct(productJson);
	}

	@Test
	public void testConsumeMessage_convertAndPush() {
		String productJson = "{\"name\":\"Pepsi\",\"type\":\"drink\",\"price\":50,\"date\":\"18-08-2019 19:32:51\"}";
		ProductDetails productDetails = new ProductDetails();
		ProductDetails productDetailsNew =  JsonUtil.convertJsonToObject(productJson, productDetails);
		eventMessageService.convertAndPushMessage(productDetailsNew, BOOT_TOPIC);
		doNothing().when(productService).saveProduct(productJson);
	}
}
