package com.deep.util;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import com.deep.model.ProductDetails;

@RunWith(SpringRunner.class)
public class JsonUtilTest {

	@Test
	public void testConvertObjectToJson() {
		ProductDetails productDetails = new ProductDetails();
		String productJson = "{\"name\":\"Pepsi\",\"type\":\"drink\",\"price\":50,\"date\":\"18-08-2019 19:32:51\"}";
		productDetails = JsonUtil.convertJsonToObject(productJson, productDetails);
		String json = JsonUtil.convertObjectToJson(productDetails);
		ProductDetails productDetailsNew = JsonUtil.convertJsonToObject(productJson, productDetails);
		assertEquals(productDetails.getName(), productDetails.getName());
		
	}

}
